from django.db import models
from django.contrib.auth.models import User

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    profile_picture = models.ImageField(upload_to='profile_pics')
    about = models.TextField(blank=True)
    location = models.CharField(max_length=100, blank=True)
    work = models.CharField(max_length=100, blank=True)
    friends_count = models.PositiveIntegerField(default=0)

class Message(models.Model):
    sender = models.ForeignKey(User, related_name='sent_messages', on_delete=models.CASCADE)
    recipient = models.ForeignKey(User, related_name='received_messages', on_delete=models.CASCADE)
    message_content = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)
class Notification(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    content = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)
    seen = models.BooleanField(default=False)
class Photo(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    image=models.ImageField(upload_to='profile',default="")
    timestamp = models.DateTimeField(auto_now_add=True)
class Friendship(models.Model):
    user = models.ForeignKey(User, related_name='user_friendships', on_delete=models.CASCADE)
    friend = models.ForeignKey(User, related_name='friend_friendships', on_delete=models.CASCADE)

class Like(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    liked_photo = models.ForeignKey(Photo, related_name='likes', on_delete=models.CASCADE,default=None)

class Sponsor(models.Model):
    image = models.ImageField(upload_to='sponsored')
    description = models.TextField()
    link = models.URLField()
class Comment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    photo = models.ForeignKey(Photo, related_name='comments', on_delete=models.CASCADE, default=None)  
    comment_text = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)
class Follower(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_followers')
    follower = models.ForeignKey(User, on_delete=models.CASCADE, related_name='follower_followings')

    class Meta:
        unique_together = ('user', 'follower')

class Following(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_followings')
    following = models.ForeignKey(User, on_delete=models.CASCADE, related_name='following_followers')

    class Meta:
        unique_together = ('user', 'following')


class Mention(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='mentions')
    mentioned_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='mentioned_in')
    post = models.ForeignKey('Post', on_delete=models.CASCADE) 
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.user.username} mentioned {self.mentioned_user.username} in post {self.post.id}"
class Post(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='post_images')
    caption = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    likes = models.ManyToManyField(User, related_name='liked_posts', blank=True)

    def __str__(self):
        return f"{self.user.username}'s post: {self.caption[:50]}"