from django.shortcuts import render, redirect,get_object_or_404
from django.views import View
from django.contrib.auth import login,authenticate
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import Post,Like,Comment,UserProfile
from django.contrib.auth.forms import AuthenticationForm
from .forms import SignInForm, CustomUserCreationForm,UserProfileForm
class IndexView(View):
    def get(self, request):
        return render(request, 'index.html')

class ProfileView(View):
    def get(self, request):
        return render(request, 'profile.html')

class NotificationView(View):
    def get(self, request):
        return render(request, 'notification.html')
class SignUp(View):
    def get(self, request):
        form = CustomUserCreationForm()
        return render(request,'signup.html', {'form': form})
    def post(self, request):
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('signin') 
        return render(request,'signup.html', {'form': form})
class SignOutView(View):
    def get(self, request):
        if request.user.is_authenticated:
            request.session.flush()
            return redirect('signin')  
        else:
            return redirect('signin')
    
class LoginView(View):
    def get(self, request):
        form = AuthenticationForm()
        return render(request,'signin.html', {'form': form})

    def post(self, request):
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect('index')
            else:
                messages.error(request, 'Invalid username or password.')

        return render(request,'signin.html', {'form': form})
class AccountSettingsView(View):

    def get(self, request, *args, **kwargs):
        context = {
            'user': request.user, 
        }
        return render(request,'account_settings.html', context)

    def post(self, request, *args, **kwargs):
        return render(request,'account_settings.html')
class PostView(View):
    def get(self, request, *args, **kwargs):
        posts = Post.objects.all()
        total_posts = posts.count()
        post_data = []
        for post in posts:
            likes_count = Like.objects.filter(post=post).count()
            comments_count = Comment.objects.filter(post=post).count()
            post_data.append({
                'post': post,
                'likes_count': likes_count,
                'comments_count': comments_count
            })

        context = {
            'user': request.user,
            'total_posts': total_posts,
            'post_data': post_data
        }
        return render(request, 'account_settings.html', context)

    def post(self, request, *args, **kwargs):
        return render(request, 'account_settings.html')
class AddProfileView(LoginRequiredMixin, View):
    def get(self, request, pk=None):
        if pk is not None:
            instance = UserProfile.objects.get(pk=pk)
            form = UserProfileForm(instance=instance)
        else:
            form = UserProfileForm()
        data = UserProfile.objects.all()
        return render(request, 'user_profile.html', {'form': form, 'data': data})

    def post(self, request, pk=None):
        if pk is not None:
            instance = UserProfile.objects.get(pk=pk)
            form = UserProfileForm(request.POST, request.FILES, instance=instance)
        else:
            form = UserProfileForm(request.POST, request.FILES)

        # Check if the user already has a profile
        if UserProfile.objects.filter(user_id=request.user.id).exists():
            # If user already has a profile, display error message
            messages.error(request, 'You already have a profile.')
            return render(request, 'user_profile.html', {'form': form})

        form.instance.user_id = request.user.id

        data = UserProfile.objects.all()

        if form.is_valid():
            form.save()
            form = UserProfileForm() 
            return render(request, 'user_profile.html', {'form': form, 'data': data})
        else:
            return render(request, 'user_profile.html', {'form': form, 'data': data})
class UpdateProfileView(View):
    def post(self, request, pk):
        user_profile = get_object_or_404(UserProfile, pk=pk)
        form = UserProfileForm(request.POST, request.FILES, instance=user_profile)
        
        if form.is_valid():
            form.save()
            messages.success(request, 'Profile updated successfully.')
            return redirect('profile_view')  
        else:
            messages.error(request, 'Failed to update profile. Please correct the errors.')
            return render(request, 'user_profile.html', {'form': form})