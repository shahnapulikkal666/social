from django import forms
from .models import UserProfile
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm

class SignInForm(AuthenticationForm):
    username=forms.CharField(widget=forms.TextInput(attrs={'class':'form-group'}))
    password=forms.CharField(widget=forms.TextInput(attrs={'class':'form-group'}))

class CustomUserCreationForm(UserCreationForm):
    email=forms.EmailField()
    phone_number=forms.CharField(max_length=30)
    

class Meta(UserCreationForm.Meta):
    model=User
    fields=('username','email','password1','password2',' phone_number')
class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ['profile_picture', 'about', 'location', 'work']
        widgets={        
            'profile_picture': forms.FileInput(),
            'about': forms.Textarea(attrs={'class':'form-group'}),
            'location':forms.TextInput(attrs={'class':'form-group'}), 
            'work':forms.TextInput(attrs={'class':'form-group'}), 
 
        }