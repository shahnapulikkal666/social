from django.urls import path
from .views import IndexView,SignOutView,ProfileView,NotificationView, SignUp, LoginView, AccountSettingsView, PostView,AddProfileView,UpdateProfileView

urlpatterns = [
    path('', LoginView.as_view(), name='signin'),
    path('index', IndexView.as_view(), name='index'),
    path('profile', ProfileView.as_view(), name='profile'),
    path('notification', NotificationView.as_view(), name='notification'),
    path('signup/', SignUp.as_view(), name='signup'),
    path('signout/', SignOutView.as_view(), name='signout'),
    path('account_settings/', AccountSettingsView.as_view(), name='account_settings'),
    path('posts/', PostView.as_view(), name='posts'),
    path('add_profile/', AddProfileView.as_view(), name='add_profile'),
    path('update_profile/', UpdateProfileView.as_view(), name='update_profile'),


   
]
